#!/usr/bin/env python


import rospy
import rostopic
import os
import rosnode
import rosservice
import rosmaster
import xmlrpclib
import roslib
import subprocess
import rospkg
from std_msgs.msg import String



cs = ""
serviceCallRelation = {}
currentReported = []
pub = rospy.Publisher('rec/service_call_handler', String, queue_size=10)
    	
def response(req):
	x =  req._connection_header
	formatedReq = formatReq(req)


	cs = str(x['service'])  + "_prime"
	y = str(x['callerid'])
	print "Current Service Debug: " + str(cs)
	serviceArgumentsValues = []
	serviceArguments = []

	Empty =  True
	if str(rosservice.get_service_args(cs)) != "":
		Empty = False
		for x in rosservice.get_service_args(cs).split(' '):
			serviceArguments.append(x)
			# Adds the values to serviceArgumentsValues 
			exec generateExecutable(x, 1)

	word = rospy.ServiceProxy(cs, rosservice.get_service_class_by_name(cs))
	
	toReturn = []
	returnArgumentsForReport = []
	returnValuesForReport = []
	if Empty == False:
	
		exec generateExecutable(None, 2, serviceArgumentsValues)
		returnArgumentsForReport.append(formatReq(toReturn)[::-1])
		exec generateExecutable(formatReq(toReturn)[::-1], 3)
	else:
		toreturn = word()

	'''	
	For the following step I must re-write the report generator to fit the format presented in the
	paper provided by Afsoon the format is on page 36. And it's the following:
		{srv:'srv_name', 
		client:'client_name, 
		server:'server_name', 
		req:{a:a_value, b:b_value}, 
		resp:{sum:sum_value}}
	Later or if you wan to be ahead of the game add delay 

	Note: Keep in mind number of returns possible 

	'''
	
	report = str(generateReport(cs,serviceArguments,serviceArgumentsValues,returnArgumentsForReport,returnValuesForReport,y))
	pub.publish(report)
	currentReported.append(report )
	return toReturn

def generateReport(service,serviceArguments, serviceArgumentsValues,returnArgumentsForReport,returnValuesForReport, client):
	x = rosservice.get_service_headers(service,rosservice.get_service_uri(service))
	toReturn = {'srv':str(x['type']), 'client':client, 'server':str(x['callerid'])}
	
	reqSubReport = {}
	respSubReport = {}
	for x in range (0,len(serviceArguments)):
		#print str(serviceArguments[x]) +', x = '+ str(x)+'. ' + str(serviceArgumentsValues[x] )
		reqSubReport[str(serviceArguments[x])] = str(serviceArgumentsValues[x])
	for x in range (0, len(returnArgumentsForReport)):
		respSubReport[str(returnArgumentsForReport[x])] = str(returnValuesForReport[x])
		#print toReturn
	toReturn['req'] = reqSubReport
	toReturn['resp'] = respSubReport
	return toReturn

def formatReq(req):
	Req = getattr(req, "__str__")

	#print 'from formatReq' + str(Req)
	toreturn = ''
	count = 0
	for line in str(Req).split(':'):
		
		count = count +1

		if count == len(str(Req).split(':')) -1: #Checks if is the last argument 
			#print str(len(str(Req).split(':'))) +', count = '+str(count)
			for x in range((len(line)-1), 0, -1):
				#print line +', Current line: 77, x = '+str(x)+', char = from:'+line[x]+':To'
				if line[x] == ' ' or line[x] == '\n':
					#print 'broke'
					break
				else:
					toreturn += line[x]
			break
		else:
			print str(len(str(Req).split(':'))) +', count = '+str(count)
			for x in range((len(line)-1), 0, -1):
				#print line +', Current line: 85, x = '+str(x)+', char = '+line[x]
				if line[x] == ' ' or line[x] == '\n':
					toreturn += ' '
					break
				else:
					toreturn += line[x]
	return toreturn

def generateExecutable(x, task, serviceArgumentsValues = None):
	if task == 1:
		return 'serviceArgumentsValues.append(req.'+x+')'

	elif task ==2:
		count = 0
		toExecute = 'toReturn = word('
		for x in serviceArgumentsValues:
			count = count +1
			if count == len(serviceArgumentsValues):
				toExecute += str(x)+')'
			else:
				toExecute+= str(x)+','
		return toExecute

	elif task == 3:
		return 'returnValuesForReport.append(toReturn.'+x+')'

def newServiceHandler(service, register):
	primeState = False
	non_prime = "OPERATOR___"
	if service.split('_')[-1] == 'prime':
		primeState = True
		non_prime = non_prime +  service.split('_prime')[0]
	
		
	currentS = service

	if register:
		#print "Checkpoint ******************"+ service +"here"
		s = rospy.Service(non_prime,rosservice.get_service_class_by_name(str(service)),response)


	return 'Passed'

def get_current_services():
	caller_ide = '/handler'
	m = xmlrpclib.ServerProxy(os.environ['ROS_MASTER_URI'])
	code, msg, val = m.getSystemState(caller_ide)
	if(code ==1):
		pubs, subs, srvs = val
	else:
		print "call failed", code, msg
	return srvs

def checkLogger(service):
	status = service.split('/')[-1] 
	if status == 'get_loggers' or status == 'set_logger_level':
		#print service.split('/')[-1] + ", checkLogger Point"
		return False
	else:
		#print service
		return True


def handle():

	rospy.init_node('handler', anonymous=False)
	rate = rospy.Rate(.5)
	currentServices = []

	for service, provider in get_current_services():
		currentServices.append(service)
		
	global serviceCallRelation
	global cs
	global pub
	global currentReported
				

	while not rospy.is_shutdown():
		print "-----------------------------------------------------------"
		for newService, Provider in get_current_services():
			if currentServices.count(newService) > 0:

				cs = newService

				print "[ ] Service: "+newService
				print "           Type: "+ rosservice.get_service_type(newService)
				print "               Arguments: " + rosservice.get_service_args(newService)
			else:
				newServiceHandler(newService,checkLogger(newService))
				print "[+] Service: "+newService
				print "            Type: "+ rosservice.get_service_type(newService) # It's ment to register the service too 
				print "                Arguments: " + rosservice.get_service_args(newService)
				print "                         Time: "+str(rospy.get_time())
		print "--------------- "+ str(rospy.get_time())+" --------------- # Services: "+ str(len(currentServices))
		
		currentServices = []
		for service, provider in get_current_services():
			currentServices.append(service)
		print currentReported
		#print str(serviceCallRelation)
		rate.sleep()



if __name__ == '__main__':
	#experimentations()
	handle()



