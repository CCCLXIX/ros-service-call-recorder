# Service call reporter for ROS systems #

The objective of this project is to generate a [Node](http://wiki.ros.org/Nodes) that is able to record service calls made in a ROS system in order to be examined later with [Daikon](https://plse.cs.washington.edu/daikon/download/doc/daikon.html)

### Requirements ###

* ROS must be installed in your system. [Instructions](http://wiki.ros.org/indigo/Installation) 


### Set-up ###


* Change directory to: */opt/ros/***version of ROS installed***/lib/python2.7/dist-packages/rospy/impl/*
* Open the file *tcpros_service.py* with any text editor (You may need to open it using sudo)
* Look for line 681 and find:
```
#!python

def __init__(self, name, service_class, handler,
                 buff_size=DEFAULT_BUFF_SIZE, error_handler=None):
```
 (Line number may vary by ROS version). Change the name parameter to Name. It should be looking like this:


```
#!python

def __init__(self, Name, service_class, handler,
                 buff_size=DEFAULT_BUFF_SIZE, error_handler=None):
```


* Right below the parameters add the following lines of code:
    
```
#!python
name = Name
status =  name.split('/')[-1] 
if status != '~'+'set_logger_level':
    if status != '~'+'get_loggers':
        if name.split('___')[0] != 'OPERATOR':
            name = name + "_prime"
        else:
            name = name.split('___')[1]
        
```

* Save and exit 
* Add service_handler.py to your project 


### Running ###

* Right after starting roscore run service_handler.py